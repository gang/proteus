
pt-noisescan -v -c analysis.toml -d device.toml -g geometry.toml ../input/run000446.raw output/noisescan

pt-align -v -c analysis.toml -d device.toml -g geometry.toml -u tel_correlations -m output/noisescan-mask.toml -n 6000 ../input/run000446.raw output/align_tel_coarse

pt-align -v -c analysis.toml -d device.toml -g output/align_tel_coarse-geo.toml -u tel_residuals -m output/noisescan-mask.toml -n 6000 ../input/run000446.raw output/align_tel_fine

pt-align -v -c analysis.toml -d device.toml -g output/align_tel_fine-geo.toml -u dut_correlations -m output/noisescan-mask.toml -n 6000 ../input/run000446.raw output/align_dut_coarse

pt-align -v -c analysis.toml -d device.toml -g output/align_dut_coarse-geo.toml -u dut_residuals -m output/noisescan-mask.toml -n 6000 ../input/run000446.raw output/align_dut_fine

pt-recon -v -c analysis.toml -d device.toml -g output/align_dut_fine-geo.toml -u gbl3d -m output/noisescan-mask.toml -n 6000 ../input/run000446.raw output/recon


